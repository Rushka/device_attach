class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.string :mac,   index: true
      t.string :token, index: true
      t.string :model, default: ''

      t.belongs_to :user, index: true

      t.timestamps null: false
    end
  end
end
