FactoryGirl.define do
  factory :device do
    mac   { Faker::Internet.mac_address }
    model { Faker::Lorem.word }
  end
end
