require 'rails_helper'

RSpec.describe API::V1::DevicesController, type: :controller do
  describe 'GET #check' do
    with :device
    with :user

    context 'device activated' do
      before do 
        get :check
        @json = JSON.parse(response.body)
      end
      it { expect(@json['active']).to eq('0') }
    end

    context 'device NOT activated' do
      before do 
        device.user = user
        device.save

        get :check
        @json = JSON.parse(response.body)
      end
      it { expect(@json['active']).to eq('1') }
      it 'respond with token' do
        expect(@json['token'].length).to eq(6) 
      end
    end
  end
end
