require 'rails_helper'
require 'devise'

RSpec.describe UsersController, type: :controller do
  describe 'GET #new_code' do
    with :user

    context 'user logged in' do
      before do
        sign_in user
        get :new_code
      end
      it { is_expected.to render_template :new_code }
    end

    context 'user NOT logged in' do
      before do
        get :new_code
      end
      it { is_expected.to redirect_to new_user_session_path }
    end
  end

  describe 'POST #attach_check' do
    context 'with valid params' do
      it 'redirects to root'
    end
    context 'with invalid params' do
      it 're-renders #new form'
    end
  end
end
