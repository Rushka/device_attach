module API::V1
  class DevicesController < ApplicationController
    def check
      find_or_init
      if @device.has_user?
        render json: { active: '1', token: 'token1' }
      else
        render json: { active: '0' }
      end
    end

    private
      def find_or_init
        @device ||= 
          Device.where(strong_params).first_or_initialize
      end

      def strong_params
        params.permit(Device::ATTRS)
      end
  end
end
