class Device < ActiveRecord::Base
  belongs_to :user

  ATTRS = :mac, :model

  validates :mac, :token, uniqueness: true

  def has_user?
    user.present?
  end

  def attach(user)
    set_token
    self.user = user
  end

  def set_token
    self.token = generate_token
  end

  def generate_token
    p mac
    p model
    p Rails.application.secrets.salt
    Digest::MD5.hexdigest(
      mac + 
      model + 
      Rails.application.secrets.salt
      )
  end
end
