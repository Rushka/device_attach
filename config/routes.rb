Rails.application.routes.draw do
  devise_for :users
  root to: "users#new_code"
  post 'attach_code', to: 'users#attach_code', as: :attach_code

  namespace :api do
    namespace :v1 do
      get 'check', to: 'devices#check', as: :check
    end
  end
end
